;; -*- mode: scheme; coding: utf-8 -*-
;; Copyright (c) 2023 Vadym Kochan <vadim4j@gmail.com>
;; SPDX-License-Identifier: MIT
#!r6rs

(library (devices lcd-char-display-i2c)
  (export make-lcd-char-display
          lcd-max-cols
          lcd-max-rows
          lcd-put-char
          lcd-put-string
          lcd-put-byte
          lcd-backlight-on
          lcd-backlight-off
          lcd-display-clear
          lcd-display-on
          lcd-display-off
          lcd-cursor-to-home
          lcd-cursor-to
          lcd-cursor-on
          lcd-cursor-off
          lcd-cursor-blink
          lcd-cursor-no-blink
          lcd-scroll-left
          lcd-scroll-right
          lcd-left-to-right
          lcd-right-to-left
          lcd-autoscroll-on
          lcd-autoscroll-off
          lcd-create-char)
  (import (rnrs)
          (devices lcd-char-display-i2c private compat)
          (devices i2c))

(define latin-1-transcoder (make-transcoder (latin-1-codec) (eol-style none)))

;; commands
(define LCD-CLEARDISPLAY   #x01)
(define LCD-RETURNHOME     #x02)
(define LCD-ENTRYMODESET   #x04)
(define LCD-DISPLAYCONTROL #x08)
(define LCD-CURSORSHIFT    #x10)
(define LCD-FUNCTIONSET    #x20)
(define LCD-SETCGRAMADDR   #x40)
(define LCD-SETDDRAMADDR   #x80)

;; flags for display entry mode
(define LCD-ENTRYRIGHT          #x00)
(define LCD-ENTRYLEFT           #x02)
(define LCD-ENTRYSHIFTINCREMENT #x01)
(define LCD-ENTRYSHIFTDECREMENT #x00)

;; flags for display on/off control
(define LCD-DISPLAYON  #x04)
(define LCD-DISPLAYOFF #x00)
(define LCD-CURSORON   #x02)
(define LCD-CURSOROFF  #x00)
(define LCD-BLINKON    #x01)
(define LCD-BLINKOFF   #x00)

;; flags for display/cursor shift
(define LCD-DISPLAYMOVE #x08)
(define LCD-CURSORMOVE  #x00)
(define LCD-MOVERIGHT   #x04)
(define LCD-MOVELEFT    #x00)

;; flags for function set
(define LCD-8-BIT-MODE #x10)
(define LCD-4-BIT-MODE #x00)
(define LCD-2-LINE     #x08)
(define LCD-1-LINE     #x00)
(define LCD-5x10-DOTS  #x04)
(define LCD-5x8-DOTS   #x00)

;; flags for backlight control
(define LCD-BACKLIGHT    #x08)
(define LCD-NO-BACKLIGHT #x00)

(define LCD-En #b00000100)
(define LCD-Rw #b00000010)
(define LCD-Rs #b00000001)

(define-record-type $lcd-char-display
  (fields
    bus
    addr
    cols
    rows
    (mutable backlight)
    func
    (mutable control)
    (mutable text-mode)))

(define (sleep-milliseconds mls)
  (sleep-seconds (/ mls 1000)))

(define (sleep-microseconds mcs)
  (sleep-milliseconds (/ mcs 1000)))

(define ($lcd-expander-write lcd byte)
  (i2c-bus-write-byte ($lcd-char-display-bus lcd)
                      ($lcd-char-display-addr lcd)
                      (bitwise-ior ($lcd-char-display-backlight lcd) byte)))

(define ($lcd-write-4-bits lcd byte)
  ($lcd-expander-write lcd byte)
  ($lcd-pulse-enable lcd byte))

(define ($lcd-pulse-enable lcd byte)
  ($lcd-expander-write lcd (bitwise-ior byte LCD-En))
  (sleep-microseconds 1)
  ($lcd-expander-write lcd (bitwise-and byte (bitwise-not LCD-En)))
  (sleep-microseconds 50))

(define ($lcd-send lcd value mode)
  ($lcd-write-4-bits lcd (bitwise-ior (bitwise-and value #xF0) mode))
  ($lcd-write-4-bits lcd (bitwise-ior
                           (bitwise-and
                             (bitwise-arithmetic-shift-left value 4)
                             #xF0)
                           mode))
)

(define (lcd-put-byte lcd byte)
  ($lcd-send lcd byte LCD-Rs))

(define (lcd-put-char lcd ch)
  (lcd-put-byte lcd (char->integer ch)))

(define (lcd-put-string lcd str)
  (let ((bv (string->bytevector str latin-1-transcoder)))
    (let loop ((i 0))
      (when (< i (bytevector-length bv))
        (lcd-put-byte lcd (bytevector-u8-ref bv i))
        (loop (+ i 1))))))

(define (lcd-command lcd byte)
  ($lcd-send lcd byte 0))

(define make-lcd-char-display
  (case-lambda
    ((bus addr cols rows)
     (let ((lcd (make-$lcd-char-display (get-i2c-bus bus) addr
                             cols rows
                             LCD-NO-BACKLIGHT
                             ;; display func
                             (bitwise-ior LCD-4-BIT-MODE
                                          LCD-1-LINE
                                          LCD-5x8-DOTS
                                          (if (> rows 1)
                                              LCD-2-LINE
                                              0))
                             ;; display control
                             (bitwise-ior LCD-DISPLAYON
                                          LCD-CURSOROFF
                                          LCD-BLINKOFF)
                             ;; text mode
                             (bitwise-ior LCD-ENTRYLEFT
                                          LCD-ENTRYSHIFTDECREMENT))))
       (lcd-initialize lcd)
       lcd))

    ((bus cols rows)
     (make-lcd-char-display bus #x27 cols rows))

    ((bus addr)
     (make-lcd-char-display bus addr 16 2))

    ((bus)
     (make-lcd-char-display bus #x27))
  )
)

(define (lcd-max-cols lcd)
  ($lcd-char-display-cols lcd))

(define (lcd-max-rows lcd)
  ($lcd-char-display-rows lcd))

(define (lcd-initialize lcd)
  (sleep-milliseconds 50)
  ($lcd-expander-write lcd ($lcd-char-display-backlight lcd))
  (sleep-milliseconds 1000)

  ;; we start in 8-bit mode
  ;; put LCD in 4-bit mode
  ($lcd-write-4-bits lcd (bitwise-arithmetic-shift-left #x03 4))
  (sleep-microseconds 4500) ;; wait min 4.1ms

  ;; second try
  ($lcd-write-4-bits lcd (bitwise-arithmetic-shift-left #x03 4))
  (sleep-microseconds 4500) ;; wait min 4.1ms

  ;; third go
  ($lcd-write-4-bits lcd (bitwise-arithmetic-shift-left #x03 4))
  (sleep-microseconds 150)

  ;; finally set to 4-bit mode
  ($lcd-write-4-bits lcd (bitwise-arithmetic-shift-left #x02 4))

  ;; set lines, font size, etc
  (lcd-command lcd (bitwise-ior LCD-FUNCTIONSET
                                ($lcd-char-display-func lcd)))

  ;; turn the display on with no cursor or blinking default
  (lcd-display-on lcd)

  ;; clear it off
  (lcd-display-clear lcd)

  (lcd-command lcd (bitwise-ior LCD-ENTRYMODESET
                                ($lcd-char-display-text-mode lcd)))

  (lcd-cursor-to-home lcd))

(define (lcd-backlight-on lcd)
  ($lcd-char-display-backlight-set! lcd LCD-BACKLIGHT)
  ($lcd-expander-write lcd 0))

(define (lcd-backlight-off lcd)
  ($lcd-char-display-backlight-set! lcd LCD-NO-BACKLIGHT)
  ($lcd-expander-write lcd 0))

(define (lcd-display-on lcd)
  ($lcd-char-display-control-set! lcd (bitwise-ior ($lcd-char-display-control lcd)
                                                   LCD-DISPLAYON))
  (lcd-command lcd (bitwise-ior LCD-DISPLAYCONTROL
                                ($lcd-char-display-control lcd))))

(define (lcd-display-off lcd)
  ($lcd-char-display-control-set! lcd (bitwise-and ($lcd-char-display-control lcd)
                                                   (bitwise-not LCD-DISPLAYON)))
  (lcd-command lcd (bitwise-ior LCD-DISPLAYCONTROL
                                ($lcd-char-display-control lcd))))

(define (lcd-display-clear lcd)
  (lcd-command lcd LCD-CLEARDISPLAY)
  (sleep-microseconds 2000))

(define (lcd-cursor-to-home lcd)
  (lcd-command lcd LCD-RETURNHOME)
  (sleep-microseconds 2000))

(define (lcd-cursor-to lcd col row)
  (define row-offsets (list #x00 #x40 #x14 #x54))
  (let ((row (if (>= row (lcd-max-rows lcd))
                 (- (lcd-max-rows 1))
                 row)))
    (lcd-command lcd (bitwise-ior LCD-SETDDRAMADDR
                                  (+ col (list-ref row-offsets row))))))

(define (lcd-cursor-on lcd)
  ($lcd-char-display-control-set! lcd (bitwise-ior ($lcd-char-display-control lcd)
                                                   LCD-CURSORON))
  (lcd-command lcd (bitwise-ior LCD-DISPLAYCONTROL
                                ($lcd-char-display-control lcd))))


(define (lcd-cursor-off lcd)
  ($lcd-char-display-control-set! lcd (bitwise-and ($lcd-char-display-control lcd)
                                                   (bitwise-not LCD-CURSORON)))
  (lcd-command lcd (bitwise-ior LCD-DISPLAYCONTROL
                                ($lcd-char-display-control lcd))))

(define (lcd-cursor-blink lcd)
  ($lcd-char-display-control-set! lcd (bitwise-ior ($lcd-char-display-control lcd)
                                                   LCD-BLINKON))
  (lcd-command lcd (bitwise-ior LCD-DISPLAYCONTROL
                                ($lcd-char-display-control lcd))))

(define (lcd-cursor-no-blink lcd)
  ($lcd-char-display-control-set! lcd (bitwise-and ($lcd-char-display-control lcd)
                                                   (bitwise-not LCD-BLINKON)))
  (lcd-command lcd (bitwise-ior LCD-DISPLAYCONTROL
                                ($lcd-char-display-control lcd))))

(define (lcd-scroll-left lcd)
  (lcd-command lcd (bitwise-ior LCD-CURSORSHIFT
                                LCD-DISPLAYMOVE
                                LCD-MOVELEFT)))

(define (lcd-scroll-right lcd)
  (lcd-command lcd (bitwise-ior LCD-CURSORSHIFT
                                LCD-DISPLAYMOVE
                                LCD-MOVERIGHT)))

(define (lcd-left-to-right lcd)
  ($lcd-char-display-text-mode-set! lcd (bitwise-ior ($lcd-char-display-text-mode lcd)
                                                     LCD-ENTRYLEFT))
  (lcd-command lcd (bitwise-ior LCD-ENTRYMODESET
                                ($lcd-char-display-text-mode lcd))))

(define (lcd-right-to-left lcd)
  ($lcd-char-display-text-mode-set! lcd (bitwise-and ($lcd-char-display-text-mode lcd)
                                                     (bitwise-not LCD-ENTRYLEFT)))
  (lcd-command lcd (bitwise-ior LCD-ENTRYMODESET
                                ($lcd-char-display-text-mode lcd))))

(define (lcd-autoscroll-on lcd)
  ($lcd-char-display-text-mode-set! lcd (bitwise-ior ($lcd-char-display-text-mode lcd)
                                                     LCD-ENTRYSHIFTINCREMENT))
  (lcd-command lcd (bitwise-ior LCD-ENTRYMODESET
                                ($lcd-char-display-text-mode lcd))))

(define (lcd-autoscroll-off lcd)
  ($lcd-char-display-text-mode-set! lcd (bitwise-and ($lcd-char-display-text-mode lcd)
                                                     (bitwise-not LCD-ENTRYSHIFTINCREMENT)))
  (lcd-command lcd (bitwise-ior LCD-ENTRYMODESET
                                ($lcd-char-display-text-mode lcd))))

(define (lcd-create-char lcd id lst)
  (let ((id (bitwise-and id #x07)))
    (assert (= (length lst) 8))
    (lcd-command lcd (bitwise-ior LCD-SETCGRAMADDR
                                  (bitwise-arithmetic-shift-left id 3)))
    (for-each
      (lambda (c)
        (lcd-put-byte lcd c))
      lst)))
)
